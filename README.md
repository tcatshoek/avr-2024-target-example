# Target example

This repository serves as an example for how to structure a project that serves as a target for the AVR challenge.

There are several guidelines targets should follow to make them work with our automation:

### Guidelines
Targets should be written in C or C++ and compile and run on ubuntu 22.04 LTS. 

Targets should communicate over stdin, stdout and stderr only.

Targets may not contain vulnerabilities hidden behind cryptographic checksums or the like. E.g., vulnerabilities 
should not be near-impossible to discover using techniques like fuzzing and symbolic execution.
It is ok if they're difficult to discover though :) 

Targets should crash once a vulnerability is triggered, for example by calling `abort()`

The target repositories should be structured in a specific way. There needs to be:
 - a folder called `inputs` with an input for the program that triggers each included vulnerability. In this example, there are 2 vulnerabilities, so there are two input files in the input folder.
 - a script called `compile.sh` that installs any needed dependencies and compiles the target binary. It should use cmake, and allow overriding of the compilers used through `CC` and `CXX` env vars so participants can compile using instrumentation if necessary (like for AFL for example).

Each target repository should have several branches:
 - a `main` branch, which contains the target with all vulnerabilities included
 - a `fix-n` branch for each vulnerability, in which this vulnerability is patched and does not crash anymore. For example, this example target has two vulnerabilities, so there will be a `fix-1` branch and a `fix-2` branch.
 - a `fix-all` branch where all the intended vulnerabilities are patched. We use this to catch unintended crashes that could potentially be triggered by the participants.

Please also see `.gitlab-ci.yml` for how the CI works and how things are packaged.