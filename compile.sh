#!/bin/bash

set -e

# Install potential dependencies here
echo "installing dependencies..."

# Script to build your code
mkdir build
cd build
cmake ..
make -j`nproc`